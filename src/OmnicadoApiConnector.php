<?php

namespace Omnicado\PublicApi;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\Dotenv\Dotenv;

class OmnicadoApiConnector
{
    private string $apiBase = "https://api.services.omnicado.com";

    /**
     * @var Client
     */
    private $client;

    public function __construct()
    {
        $this->client = new Client();

        $dotenv = new Dotenv();
        $dotenv->load(__DIR__ . '/../.env');

        $this->apiBase = $_ENV['OMNICADO_API'] ?? $this->apiBase;
    }

    public function getRequestHeaders(array $data): array
    {
        $requestHeaders = [
            'headers' => [
                'X-OMNICADO-TOKEN' => $_ENV['OMNICADO_API_KEY']
            ],
        ];

        if (!empty($data)) {
            $requestHeaders['json'] = $data;
        }

        return $requestHeaders;
    }

    public function me(): string
    {
        return (string)$this->getRequest('/me')->getBody();
    }

    public function marketplaces()
    {
        return (string)$this->getRequest('/marketplaces')->getBody();
    }

    public function getRequest(string $endpoint, array $data = []): Response
    {
        return $this->client->get($this->apiBase . $endpoint, $this->getRequestHeaders($data));
    }

    public function sendRequest(string $endpoint, array $data = []): Response
    {
        return $this->client->post($this->apiBase . $endpoint, $this->getRequestHeaders($data));
    }
}
