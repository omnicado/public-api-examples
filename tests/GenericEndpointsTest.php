<?php

namespace Omnicado\PublicApi\Tests;

use Omnicado\PublicApi\OmnicadoApiConnector;
use PHPUnit\Framework\TestCase;

final class GenericEndpointsTest extends TestCase
{
    public function testMeEndpoint()
    {
        $omnicadoApiConnector = new OmnicadoApiConnector();

        $response = $omnicadoApiConnector->me();

        $this->assertJson($response);
    }

    public function testMarketplacesEndpoint()
    {
        $omnicadoApiConnector = new OmnicadoApiConnector();

        $response = $omnicadoApiConnector->marketplaces();

        $this->assertJson($response);
    }

    public function testMarketplacesContainAllegroEndpoint()
    {
        $omnicadoApiConnector = new OmnicadoApiConnector();

        $response = $omnicadoApiConnector->marketplaces();

        $marketplaces = json_decode($response, true);

        $this->assertContains('allegroPl', $marketplaces);
    }
}
